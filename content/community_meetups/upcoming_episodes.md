---
widget: blank
headless: true  # This file represents a page section.
title: "Upcoming Episodes" 
subtitle: |
    Register today to join the conversations!
weight: 20

design:
    columns: "1"
---


<div class="row">
   <h3> <a href="../events-rsse-africa/2024-11-14/">Episode 2: Enabling Reproducibility through Research Code</a></h3>
</div>

14 November 2024 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Reproducibility+and+Research+Software&iso=20241114T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<a href="https://us06web.zoom.us/meeting/register/tZcpcu-ppjstGNJwEt1aKRjR6r2aC7Q4qE9Y" target="_blank"><img src="../static-images/register-button.png"></a>


<div class="row">
    <h3> <a href="../events-rsse-africa/2024-12-12/">Episode 3: Opening Up Research Code</a></h3>
</div>

12 December 2024 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?iso=20241212T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<div class="row">
    <h3> <a href="../events-rsse-africa/2025-01-23/">Episode 4: Documentation for Research Code</a></h3>
</div>

23 January 2025 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Documentation+for+Research+Code&iso=20250123T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<div class="row">
    <h3> <a href="../events-rsse-africa/2025-02-20/">Episode 5: Testing Research Code</a></h3>
</div>

20 February 2025 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Testing+Research+Code&iso=20250220T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<div class="row">
    <h3> <a href="../events-rsse-africa/2025-03-20/">Episode 6: Research Software Funding</a></h3>   
</div>

20 March 2025 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Research+Software+Funding&iso=20250320T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

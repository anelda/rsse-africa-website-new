---
widget: blank
headless: true  # This file represents a page section.
title: "Our Partners" 
subtitle: |
    <br> The series is a collaboration between Talarify, RSSE Africa, RSE Asia, the African Reproducibility Network, and the Research Software Alliance. <br>
    
weight: 50
design:
    columns: "1"
---

<div class="container">
    <div class="row">
        <div class="col-3">
            <p align="center">
                <a href="https://rsse.africa"><img src="rsse-africa-logo.png" ></a>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                <a href="https://rse-asia.github.io/RSE_Asia/"><img src="rse-asia-logo.png" ></a>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                <a href="https://africanrn.org/"><img src="aren-logo.png" ></a>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                <a href="https://www.researchsoft.org/"><img src="resa-logo.png" ></a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p align="center">
                <a href="https://www.talarify.co.za/"><img src="talarify-logo.png" width="30%"></a>
            </p>
        </div>
    </div>
</div>
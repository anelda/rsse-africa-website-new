---
widget: blank
headless: true  # This file represents a page section.
title: "Enabling Open Science Through Research Code" 
subtitle: |
    <br> Are you a researcher who writes code as part of your work? Join us for the upcoming six-part series to learn good practices, grow your network, and showcase your work! <br>
    
weight: 10
design:
    columns: "1"
---

<a href="#upcoming_events"><img src="community-meetups-2024-2025.png"></a>
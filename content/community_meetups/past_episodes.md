---
widget: blank
headless: true  # This file represents a page section.
title: "Past Episodes" 
subtitle: |
    If you missed it, you can still access recordings and resources!
weight: 30

design:
    columns: "1"
---

<div class="row">
    <h3> <a href="../events-rsse-africa/2024-10-10/">Episode 1: A Conversation with Researchers Who Code</a></h3>         
</div>

10 October 2024 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Research+Software+and+Systems+in+Africa+and+Asia&iso=20241010T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<strong>* Our inaugural meetup coincided with <a href="https://society-rse.org/international-rse-day-october-10th-2024/">International Research Software Engineering Day!</a></strong>



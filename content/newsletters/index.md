---
# Leave the homepage title empty to use the site title
title: Newsletter Archive
date: 2023-04-16
type: page
---

### 2024

📆 [August 2024](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=8863f78b44) (English/Français)

📆 [June 2024](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=9b8c92f811) (English/Français)

📆 [April 2024](http://mailchi.mp/talarify/rsse-africa-april2024) (English)


📆 [February 2024](http://mailchi.mp/talarify/rsse-africa-february2024) (English)


### 2023

📆 [November 2023](https://mailchi.mp/talarify/rsse-africa-november2023) (English)


📆 [October 2023](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=03ef256cba) (English)


📆 [May 2023](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=e6373bc5b4) (English)


📆 [April 2023](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=f21061c98a) (English)


📆 [March 2023](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=26d810e0eb) (English)


📆 [February 2023](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=626fad8d96) (English)


### 2022

📆 [December 2022](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=4c260b0cdf) (English)

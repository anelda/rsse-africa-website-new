---
# Display name
title: Dr Kate Huddlestone

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Senior Lecturer"

user_groups: ["Episode 2 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Department of General Linguistics, Stellenbosch University, South Africa
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I am a senior lecturer  in the Department of General Linguistics at Stellenbosch University, the managing editor for Stellenbosch Papers in Linguistics Plus (SPiL Plus) and deputy chairperson for the Southern African Linguistics and Applied Linguistics Society (SALALS). My research focuses primarily on South African Sign Language (SASL) but I also work on aspects of the syntax and pragmatics of Afrikaans and South African English. I use ELAN to annotate SASL data collected from deaf signers. I use R to analyse the effect of linguistic and sociolinguistic variables on lexical variation, language structure and language use.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://www.researchgate.net/profile/Kate-Huddlestone
- icon: envelope
  icon_pack: fa
  link: mailto:katevg@sun.ac.za

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---


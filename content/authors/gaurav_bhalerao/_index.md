---
# Display name
title: Dr Gaurav Bhalerao

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Postdoctoral Research Associate"

user_groups: ["Episode 1 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Nuffield Dept of Clinical Neurosciences, University of Oxford, UK
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I am a postdoctoral researcher at the Wellcome Centre for Integrative Neuroimaging (WIN), developing tools and algorithms for analyzing multimodal MRI data. My research focuses on detecting predictive brain biomarkers, creating machine learning applications for automated detection, and developing neuroimaging pipelines. As the UKRN local network lead, I work with others to expand the Reproducible Research Oxford community through events at the University of Oxford. I also hold a Software Sustainability Institute (SSI) fellowship, engaging with the research software community and promoting best practices through interdisciplinary approaches.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://linkedin.com/in/gauravbhalerao/
- icon: building-user
  icon_pack: fa
  link: https://www.ndcn.ox.ac.uk/team/gaurav-bhalerao
- icon: user
  icon_pack: fa
  link: https://orcid.org/0000-0002-3325-3413
- icon: envelope
  icon_pack: fa
  link: mailto:gvbhalerao591@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "gvbhalerao591@gmail.com"

# Highlight the author in author lists? (true/false)
highlight_name: true



---


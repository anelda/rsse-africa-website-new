---
# Display name
title: Anelda van der Walt

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Digital Research Enabling Consultant"

user_groups: ["Talarify", "Organising team", "Programme Committee"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Talarify
  - url: https://talarify.co.za

# Short bio (displayed in user profile at end of posts)
bio: 
  

# Interests to show in About widget
interests:
  - Anelda is the founder of Talarify, a South African consulting company that was established in 2015. She uses her wealth of knowledge about the local research ecosystem, and her experience in interdisciplinary research and open science to create contextualised capacity and community development programmes. Her focus is mostly on capacity development in the digital research infrastructure space including open science, reproducibility and interdisciplinarity.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/aneldavanderwalt/?originalSubdomain=za
- icon: envelope
  icon_pack: fa
  link: "mailto:anelda@talarify.co.za"
- icon: user
  icon_pack: fa
  link: https://orcid.org/0000-0003-4245-8119


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---


---
title: "🇿🇼 February 2024: Meet Tadiwanashe Gutsa"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our first community member spotlight for 2024! In this month's edition we're introducing Tadiwanashe Gutsa from Zimbabwe.

# Link this post with a project
projects: []

# Date published
date: '2024-02-27T00:00:00Z'


# Date updated
lastmod: '2024-02-27T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Tadiwanashe Gutsa'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Tadiwanashe Gutsa

tags:
  - Community members
  - Agricultural Engineering
  - Zimbabwe

categories:
  - Spotlights
  - Zimbabwe
---

Our February spotlight shines on Tadiwanashe Gutsa from Zimbabwe. Tadiwanashe is a research assistant and PhD Candidate at the University of Kwazulu-Natal in South Africa.

## In short
---

- __Preferred name and surname__: Tadiwanashe Gutsa
- __Affiliation__ (where do you work or study): University of Kwazulu-Natal
- __Role__:  Research Assistant and PhD Candidate
- __Email__: tadigutsah@gmail.com
- __LinkedIn__: https://www.linkedin.com/in/tadiwanashe-gutsa-060b97110/

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Wenge Mambo](https://www.youtube.com/watch?v=F0flpGoVTFE) – _Oliver Mtukudzi_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

I have a background in Agricultural Engineering from the University of Zimbabwe.

#### Do you have postgrad qualifications? In what area?

I have a Masters in Civil Engineering from the University of Kwazulu-Natal

#### What is the title of your current role?

Junior Researcher under the SARCHI in Waste Management and Climate Change and 4th year PhD candidate in Civil Engineering at the University of Kwazulu-Natal.


#### Give a one-sentence summary of what you do in this role

My research focus area is on  plastic pollution in riverine environments and as Junior Researcher I design field protocols, manage data and project instrumentation for monitoring plastic pollution in river systems and assist in the development of strategies aimed at mitigating plastic pollution in riverine environments.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

3.5 hrs a day 

#### How many projects do you work on?

3

#### Give some keywords that summarise the topics of your projects.

- Riverine plastic hotspots
- Plastic transport
- River to Ocean plastic emissions


#### How often do you work with non-coding researchers?

less often.

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

Provide education, foster learning and research


#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

In my research group - 2

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

The Carpentries

#### What training has had an impact on your current career?

The Carpentries 


{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

Researcher

{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

Administrative challenges and funding for personal development programs

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Coding - mapping and data analysis.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

  Personal development programs and software licensing

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

Looking forward to publish scientific articles and submitting my PhD

{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

Be confident and always believe in yourself

{{< /spoiler >}}





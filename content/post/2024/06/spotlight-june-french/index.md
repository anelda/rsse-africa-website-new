---
title: "🇿🇦 Juin 2024 : Rencontrez Mthetho Vuyo Sovara "

subtitle: 

# Summary for listings and search engines
summary:  Bienvenue 👋 sur notre coup de projecteur sur les membres de la communauté d'août 2024 ! Dans l'édition de ce mois-ci, nous vous présentons Mthetho Vuyo Sovara d'Afrique du Sud.

# Link this post with a project
projects: []

# Date published
date: '2024-06-18T00:00:00Z'


# Date updated
lastmod: '2024-06-18T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: "Crédit d'image: Mthetho Vuyo Sovara"
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Mthetho Vuyo Sovara

tags:
  - Membres de la communauté
  - Dynamiques de l'océan et du climat
  - Afrique du Sud

categories:
  - Spotlights
  - South Africa
---

Notre projecteur du mois de juin est braqué sur Mthetho Vuyo Sovara d'Afrique du Sud. Mthetho est un scientifique de soutien à la recherche au 'Centre for High-Performance Computing' (CHPC), qui fait partie du 'Council for Scientific and Industrial Research' (CSIR). Il est également doctorant à l'Université de Cape Town.

📣 **Des nouvelles passionnantes** 

Ce coup de projecteur sur la communauté est également disponible en [anglais](../../../2024/06/spotlight-june/).

## En bref
---

- __Nom et prénom souhaités__: Mthetho Vuyo Sovara
- __Affiliation__ (où travaillez-vous ou étudiez-vous): 'Centre for High-Performance Computing' (CHPC) au 'Council for Scientific and Industrial Research' (CSIR)
- __Role__: Scientifique de soutien à la recherche
- __Email__: msovara@csir.co.za
- __LinkedIn__: https://www.linkedin.com/in/mthetho-sovara-3a939272/
- __Twitter(X)__: https://x.com/MSovara
- __ORCID ID__: [0000-0002-0498-0179](https://orcid.org/0000-0002-0498-0179)

---


{{< spoiler text="__:musical_score: Quelle est votre chanson préférée ?__" >}}

:sound: [Be Still & Know](https://www.youtube.com/watch?v=Ia7s2OE4PC4) – _Housefires feat. DOE_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Pouvez-vous décrire votre parcours et votre rôle actuel ?__" >}}

#### Qu’as-tu étudié et où ?

Mes études de premier cycle en sciences de l'environnement (licence avec mention) ont été effectuées à l'université Walter Sisulu (WSU), campus de Mthatha.

#### Avez-vous des diplômes de troisième cycle ? Dans quel domaine ?

Actuellement, je suis titulaire d'un Master en Sciences (MSc) en Dynamique des Océans et du Climat de l'Université de Cape Town, où je poursuis également mon doctorat.

#### Quel est le titre de votre rôle actuel ?

Je suis actuellement chercheur en support de recherche pour les domaines des sciences du système terrestre et de l'astrophysique au Centre for High-Performance Computing (CHPC).

#### Donnez un résumé en une phrase de ce que vous faites dans ce rôle

J'assiste les scientifiques dans l'utilisation des ressources informatique du CHPC.



{{< /spoiler >}}

{{< spoiler text="__:briefcase: Décrire une journée de travail type... Développez spécifiquement le temps que vous passez à coder__" >}}

#### Combien de temps passez-vous à coder ?

Une part importante (> 60 %) de ma journée est consacrée à l'examen des codes des utilisateurs et au débogage de leurs problèmes de calcul haute performance (HPC). Cela implique généralement la désinfection des environnements HPC, la construction et l'installation de logiciels simples à complexes, la résolution d'erreurs d'exécution et l'aide à l'optimisation des flux de travail des utilisateurs. De plus, j'étudie divers modèles d'applications météorologiques et climatiques afin de mieux comprendre leurs besoins système et leurs flux de travail, car je suis également impliqué dans divers projets de recherche.

#### Sur combien de projets travaillez-vous ?

Au CHPC, je soutiens environ 40 programmes de recherche dans les domaines des sciences du système terrestre et de l'astrophysique. Je participe également activement à diverses activités de développement des ressources humaines du CHPC telles que :

1. La compétition étudiante sur les clusters
2. Le projet SADC HPC Ecosystems
3. L'engagement scientifique du CHPC (leadership stratégique du NICIS)
4. Les écoles d'été et d'hiver du CHPC
5. Le projet Codage, programmation et robotique du ministère national de la science et de l'innovation
6. Les activités de SADC Cyber Infrastructure et le changement climatique régional
7. Le programme de sensibilisation des écoles du CHPC

#### Donnez quelques mots-clés qui résument les thèmes de vos projets.

HPC, Matériel informatique, Réseau, Logiciel, Calcul parallèle, Analyse comparative, Recherche scientifique et Applications informatiques, Modèle de recherche et de prévision météorologique, Python, Intelligence artificielle et apprentissage automatique, Visualisation, Sensibilisation, Linux, Scratch.

#### À quelle fréquence travaillez-vous avec des chercheurs non-codeurs ?

Je collabore avec des chercheurs non-codeurs autant que possible lorsque l'occasion se présente. Dans le cadre de mes fonctions, je travaille à la mise en place d'une équipe de réseau de recherche composée de personnes issues d'établissements d'enseignement supérieur sud-africains historiquement défavorisés. Grâce à l'initiative du Weather and Climate Modelling Group (WCMG), mon objectif est de co-encadrer des étudiants en spécialisation, en leur présentant les méthodes de recherche et de prévision météorologique numérique, la modélisation et le HPC.

De plus, dans ma communauté de Silverton, Pretoria, dans une église locale, une fois par an, je forme des apprenants d'horizons divers au matériel informatique, à Linux et à la programmation par blocs et Microbit, en mettant l'accent sur la robotique. Grâce à l'ONG Ulwazi Digital Innovators, nous ciblons le développement des compétences en codage, en robotique et en cybersécurité.


{{< /spoiler >}}

{{< spoiler text="__:school: Parlez-nous davantage de l’organisation dans laquelle vous travaillez.__" >}}

#### Quel est leur objectif premier ?

Le CHPC est le centre national de calcul scientifique d'Afrique du Sud, avec environ 30 employés. Nous servons la communauté de recherche en Afrique du Sud et sur le continent africain, ainsi que quelques programmes internationaux. Nos points forts continentaux et internationaux incluent le soutien aux pays partenaires du SKA, l'expérience ALICE du CERN et le projet SADC HPC Ecosystems.

#### Combien de personnes dans votre organization sont impliquées dans le développement de logiciels de recherche (un coup de pouce est acceptable) ?

Très peu, peut-être deux ou trois personnes mais je peux me tromper. Actuellement, nous nous concentrons davantage sur le déploiement et le support de logiciels de recherche open-source et commerciaux existants, plutôt que sur leur développement interne. L'Afrique du Sud souffre d'un manque reconnu d'expertise locale pour entreprendre de tels efforts, bien que de nombreux modèles commerciaux ne soient pas configurés pour nos régions spécifiques. Les leaders d'opinion en Afrique du Sud entament des discussions sur la façon de favoriser le développement de logiciels adaptés à l'Afrique, la collaboration interdisciplinaire étant identifiée comme une exigence cruciale pour atteindre cet objectif.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Où obtenez-vous une formation et un soutien ?__" >}}

#### De quelles communautés de pratique faites-vous partie ?

Avant de rejoindre le CHPC, je participais activement aux initiatives de la Société sud-africaine des scientifiques de l'atmosphère (SASAS). Au CHPC, je suis impliquée dans la collaboration CHPC-Académie des sciences d'Afrique du Sud (ASSAf), qui a une large portée concernant les communautés de pratique. À l'échelle internationale, j'ai contribué aux initiatives d'ingénieur logiciel de recherche (RSE) lors de la conférence internationale sur le calcul haute performance (ISC) en 2023 et 2024. Récemment, j'ai également rejoint la communauté RSSE-Afrique.

#### Quelle formation a eu un impact sur votre carrière actuelle ?

Il doit s'agir du MOOC du Centre européen pour les prévisions météorologiques à moyen terme (ECMWF) sur "L'apprentissage automatique en matière de météo et de climat".

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Vous considérez-vous comme un universitaire, un chercheur, un ingénieur logiciel, un technicien… ? Tout? Autre chose? Un mélange d'un ou deux termes ?__ " >}}

À ce stade de ma carrière, je suis chercheur et l'ingénierie logicielle scientifique joue un rôle central dans mon travail. Sans connotation négative, je me considère comme une " touche-à-tout et maître dans un domaine ". Dans le contexte de la quatrième révolution industrielle (4IR), la multidisciplinarité est considérée comme un avantage.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: À quels types d’obstacles êtes-vous confrontés dans votre travail ?__ " >}}

En Afrique du Sud, il n'existe actuellement aucun cursus universitaire formel pour le développement des compétences de RSE (Research Software Engineer). Une grande partie de nos progrès dans ce domaine est basée sur l'expérience pratique. De plus, les opportunités pour les RSE africains de poursuivre leur développement à l'international sont limitées en raison de notre non-participation aux initiatives internationales visant à faire progresser les compétences en logiciels de recherche. J'ai souvent des difficultés à accéder aux formations internationales.

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: Quelle partie de votre travail appréciez-vous le plus ?__" >}}

Dans mon rôle, j'apprécie l'exposition à la recherche scientifique computationnelle complexe et de haute qualité. Je fais un parallèle avec l'époque d'Albert Einstein au bureau des brevets suisse, où l'exposition à de nouvelles idées a probablement influencé sa formulation de questions scientifiques. De même, mon poste actuel dicte le type de recherche auquel je choisis de participer.

{{< /spoiler >}}

{{< spoiler text="__:sos: Qu’est-ce qui vous faciliterait la tâche et vous soutiendrait dans votre travail ?__" >}}

Un effort vers une véritable mondialisation, où une plus grande intégration à l'échelle mondiale offrirait aux RSE d'Afrique du Sud et d'Afrique entière des opportunités d'apprendre et d'exceller dans ce domaine dynamique.

{{< /spoiler >}}

{{< spoiler text="__:telescope: Qu’attendez-vous avec impatience cette année ?__" >}}

Cette année, je concentre mes efforts sur la production de résultats de recherche révolutionnaires sur la saisonnalité et la variabilité à long terme des températures et des précipitations au Lesotho, en collaboration avec les services météorologiques du Lesotho. Ce projet intègre de nouveaux outils, notamment l'apprentissage automatique, pour améliorer la prévisibilité du temps et du climat au Lesotho.

De plus, livrer discours d'ouverture a une conférence en tant qu'intervenant principal à l'atelier RSE@ISC et participer au BoF ISC Outreach en mai 2024 à l'ISC'24 ont été des étapes importantes pour moi, offrant des informations précieuses et des opportunités de développement professionnel grâce aux interactions avec un public international.

À l'avenir, la Conférence nationale du CHPC en décembre s'annonce exceptionnelle avec des projets passionnants tels que le symposium sur le codage et la robotique, ainsi que des mises à jour sur la recherche computationnelle de pointe à travers le monde. Se déroulant dans la magnifique ville de Gqeberha, cette conférence sera sans aucun doute un moment fort de l'année.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Veuillez partager les conseils de carrière les plus utiles que vous avez reçus et que vous souhaitez partager avec d'autres Africains occupant des postes similaires.__" >}}

En fait, c'est mon propre adage, bien qu'il ne soit pas unique, mais je crois fermement qu'à l'ère de la 4ème révolution industrielle, la multidisciplinarité est considérée comme un avantage.

{{< /spoiler >}}





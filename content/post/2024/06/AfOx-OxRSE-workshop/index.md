---
title: "AfOx, OxRSE, and RSSE Africa Join Hands for Landmark RSE Workshop"
subtitle: 

# Summary for listings and search engines
summary: RSSE Africa recently had the fantastic opportunity to participate in the AfOx - OxRSE half-day workshop, where we shared insights about our community and its mission.🌍🎯

# Link this post with a project
projects: []

# Date published
date: '2024-06-28T00:00:00Z'

# Date updated
lastmod: '2024-06-28T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Anelda van der Walt (https://zenodo.org/records/12515927)'
  focal_point: ''
  placement: 
  preview_only: false



authors:
  - Mireille Grobbelaar
  - Oxford RSE Group
  - Anelda van der Walt
  - Richard Dushime
  

tags:
  - University of Oxford
  - Africa Oxford Initiative
  - ReSA

categories:
  - University of Oxford
  - Africa Oxford Initiative
  - ReSA
---

RSSE Africa recently had the fantastic opportunity to participate in the AfOx - OxRSE half-day workshop, where we shared insights about our community and its mission.

The [Africa Oxford (AfOx) Initiative](https://www.afox.ox.ac.uk/africa-oxford-initiative) is a cross-university platform dedicated to fostering partnerships between the University of Oxford and African institutions. The AfOx [Visiting Fellowship Programme](https://www.afox.ox.ac.uk/afox-visiting-fellowship-programme) supports African scholars across various disciplines. The fellows spend a year affiliated with Oxford, working on their chosen projects while visiting the university.

{{< cta cta_text="Learn more about AfOx" cta_link="https://www.afox.ox.ac.uk/africa-oxford-initiative" cta_new_tab="true" >}}

The [Oxford Research Software Engineering Group](https://www.rse.ox.ac.uk) (OxRSE) is a central group at the University of Oxford that provides software development and consultation for research projects throughout the University. They have expertise in various aspects of research, including research software development, setup for research software development infrastructure, consultations on best practices in research software, code review, and research software practice training

{{< cta cta_text="Learn more about OxRSE" cta_link="https://www.rse.ox.ac.uk" cta_new_tab="true" >}}

The 2023-24 AfOx Visiting Fellows spent May and June at Oxford, and as part of their visit, they attended an AfOx-OxRSE workshop held on June 24th. The workshop aimed to offer an introduction to research software engineering and its impact on academic research, as well as a space for experienced RSEs to share their expertise.

Dr. David Kerr, Programme Manager at AfOx, kicked off the workshop by welcoming the attendees and providing background on the Visiting Fellows Programme. Dr. Mihaela Duta, principal RSE at the OxRSE group, then provided an overview of what RSE entails.

Dr. Martin Robinson, head of the OxRSE group, provided insights into the group's establishment in December 2018 and its self-sustaining funding model, which has successfully supported over 100 projects and grants across the university. He showcased several innovative projects led by the group, including [PyBamm](https://pybamm.org/), [Galv](https://github.com/galv-team), [PyBop](https://github.com/pybop-team/PyBOP), and [Language Screen](https://oxedandassessment.com/languagescreen/). 

Anelda van der Walt, director of [Talarify](https://www.talarify.co.za/), the African [Research Software Alliance](https://www.researchsoft.org/) (ReSA) Community Engagement Partner, presented RSSE Africa’s goals. She highlighted the diversity and richness of RSE work already occurring on African soil and addressed the challenges in community engagement, particularly the lack of identification with the term RSE. Anelda encouraged AfOx fellows to get involved, contribute to the community, and leverage collective expertise to tackle local and global challenges through innovative research software solutions. To learn more about African-specific RSE and systems engineering challenges, see [this article](https://www.researchsoft.org/blog/2023-12/) published on the ReSA blog. 

{{< cta cta_text="Learn more about RSSE Africa" cta_link="https://rsse.africa" cta_new_tab="true" cta_alt_text="Learn more about ReSA" cta_alt_link="https://researchsoft.org/" cta_new_tab="true" >}}


Dr. John Brittain also from the OxRSE group presented  [Global.health](https://global.health/), an international project with Oxford as a key collaborator. Global.health aims to provide an accessible global repository and visualisation platform for real-time, de-identified epidemiological case data on infectious diseases and emerging outbreaks. This platform is particularly crucial for Africa, as it tracks diseases significant to African communities, such as Ebola and Mpox.

The workshop fostered an open dialogue where attendees shared their perspectives, challenges, and suggestions for building RSE capacity. A recurring theme was the relevance and identification with the RSE title in African universities, which was new or unfamiliar to the AfOx fellows in the audience. The distinction between Rse (Research software engineer) and rSE (research Software Engineer) was particularly useful, emphasising that individuals could come from strong research backgrounds with coding skills (Rse) or from software development with a research interest (rSE). The goal is to attract experts in one area with some proficiency in the other, and a desire to grow.

Researchers from the audience with limited software knowledge but a curiosity to learn noted their encounters with tools like GitHub in their daily work but highlighted their lack of basic skills to use these effectively. Open educational resources, such as [training materials](https://train.oxrse.uk/material) from the OxRSE group, were recommended as valuable starting points for academics looking to enhance their software skills.

This discussion highlighted the importance of communities of practice like RSSE Africa, which support academics and RSEs at various levels of software training. Making RSE groups more common and accessible in African universities was a key topic. We discussed the challenge of obtaining university responses on research software work and the need for AfOx fellows (and Africans in similar opportunities) to promote RSE at their home institutions. Expanding the network is crucial for understanding software development across the continent. In this context, we discussed the additional effort required to reach the whole continent, and the challenges presented by cultural and technological diversity. South African institutions have a stronger presence in RSE communities relative to other African nations, and while they can serve as a model for inclusion, that model must be adapted to each new context to make sure communities are properly empowered.

Attendees were encouraged to join RSE communities, even if they are not RSEs, to better understand the role of software in research. The diversity within the RSE community is a strength, and events like the upcoming [RSECon24](https://rsecon24.society-rse.org/) are welcoming to researchers with varying technical expertise. These events expose participants to a variety of research disciplines, all unified by the use of research software.

{{< cta cta_text="Learn more about RSECon24" cta_link="https://rsecon24.society-rse.org/" cta_new_tab="true" cta_alt_text="Attend RSE Worldwide @ RSECon24 FREE" cta_alt_link="https://rsecon24.society-rse.org/programme/rse-worldwide/" cta_new_tab="true" >}}


The AfOx RSE workshop was a testament to the power of collaboration and knowledge sharing. It was an invaluable opportunity for RSSE Africa to engage with the AfOx community, articulate our mission, and learn from the diverse experiences of the fellows. We are excited about the future and look forward to continued collaboration and growth within the research software ecosystem across Africa.
















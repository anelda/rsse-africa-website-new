---
title: "🇨🇩 Avril 2024 : Rencontrez Richard Dushime"

subtitle: 

# Summary for listings and search engines
summary:  Bienvenue 👋 sur notre coup de projecteur sur les membres de la communauté d'avril 2024 ! Dans l'édition de ce mois-ci, nous vous présentons Richard Dushime de la République Démocratique du Congo.

# Link this post with a project
projects: []

# Date published
date: '2024-04-27T00:00:00Z'


# Date updated
lastmod: '2024-04-27T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: "Crédit d'image: Richard Dushime"
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Richard Dushime

tags:
  - Membres de la communauté
  - Open Source
  - RSEAA24
  - La République Démocratique du Congo

categories:
  - Spotlights
  - The Democratic Republic of the Congo
---

Notre projecteur du mois d'avril est braqué sur Richard Dushime de la République Démocratique du Congo. Richard est le secrétaire de RSEAA24, l'unconference RSE Asie-Australie, et un contributeur open source.

📣 **Des nouvelles passionnantes** 

Ce coup de projecteur sur la communauté est également disponible en [anglais](../../../2024/04/spotlight-april/).

## En bref
---

- __Nom et prénom souhaités__: Richard Dushime
- __Affiliation__ (où travaillez-vous ou étudiez-vous): Comité d'organisation de 'Research Software Engineer Asia Australia' (RSEAA24) au nom de RSE Australia New Zealand et RSE Asia.
- __Role__: Secretary
- __Email__: mudaherarich@gmail.com 
- __LinkedIn__: https://www.linkedin.com/in/richard-dushime/
- __Twitter(X)__: https://twitter.com/RichardDushime
- __Website__: https://richarddushime.netlify.app/ 

---


{{< spoiler text="__:musical_score: Quelle est votre chanson préférée ?__" >}}

Eh bien, je n’en ai peut-être pas de spécifique, mais en général, la musique lente est ma préférée, et cela dépend de mon humeur de toute façon.

{{< /spoiler >}}

{{< spoiler text="__:books: Pouvez-vous décrire votre parcours et votre rôle actuel ?__" >}}

#### Qu’as-tu étudié et où ? 

J’ai obtenu une licence en systèmes d’information d’entreprise à l’Université Bugema de Kampala, en Ouganda, et je cherche actuellement à être admise à un programme de maîtrise en science des données ou en informatique.


#### Quel est le titre de votre rôle actuel ? 

Actuellement, j’occupe le poste de secrétaire à RSEAA24, la non-conférence RSE Asia Australia, et je suis un contributeur open-source.

#### Donnez un résumé en une phrase de ce que vous faites dans ce rôle

En tant que secrétaire de RSEAA24, je coordonne et participe aux réunions hebdomadaires, je maintiens le site web, j'exécute les tâches qui me sont déléguées, je mets à jour la documentation et je veille à l'amélioration continue et au respect des normes en matière de diversité, d'équité et d'inclusion. En outre, en tant que contributeur de logiciels open source, je participe à divers projets tels que The Turing Way, Forrt, Student-intern-organiser à WEHI, et bien d'autres encore.

#### Comment vous êtes-vous impliqué dans les communautés RSE ?

Mon parcours vers le génie logiciel de recherche (RSE) a commencé grâce à mon implication dans les communautés open source. C'est grâce à ces communautés que j'ai rencontré des mentors comme Elisee Jafsia, qui m'a fait découvrir des initiatives comme l'Open Life Science Mentorship Training. Collaborer avec des individus comme Rowland Mosbergen sur des projets open source a encore alimenté mon intérêt. Finalement, j'ai découvert les communautés et les événements RSE, tels que la non-conférence RSEAA23. Participer activement au comité d’organisation du RSEAA23 m’a permis de nouer des liens avec des RSE et d’explorer des opportunités dans le domaine.

#### Qu'est-ce qui vous a inspiré à vous impliquer dans la construction de communautés ?

Mon inspiration pour m’impliquer dans le développement communautaire découle des défis personnels auxquels j’ai été confronté au cours de mes premières années universitaires. Issu d'un milieu francophone, j'ai d'abord eu du mal à m'adapter aux activités scolaires menées en anglais. Cependant, rejoindre un club au cours de ma deuxième année m’a permis de bénéficier d’un réseau de soutien et m’a aidé à explorer des opportunités au-delà de l’université. Reconnaissant les avantages du soutien communautaire, j’ai approfondi la compréhension et la construction des communautés. Finalement, je suis devenu responsable du Google Developer Student Club (GDSC), qui a ouvert les portes de diverses communautés mondiales. Mon inspiration réside dans l’entraide et les bénéfices favorisés au sein des communautés.

#### Qu’avez-vous appris en contribuant à des projets Open Source ?

Contribuer à des projets open source a été une formidable expérience d’apprentissage. Cela m'a appris à partager des connaissances et à soutenir les autres. J'ai également appris à travailler dans un environnement dynamique où les choses peuvent changer rapidement. C'est un processus d'apprentissage continu qui a enrichi mes compétences et élargi mes perspectives.



{{< /spoiler >}}

{{< spoiler text="__:briefcase: Décrire une journée de travail type... Développez spécifiquement le temps que vous passez à coder__" >}}

#### Combien de temps passez-vous à coder ?

Je me concentre principalement sur le développement de logiciels open source, ce qui implique de coder la plupart du temps.

#### Sur combien de projets travaillez-vous ?

Actuellement, je contribue à divers projets, dont The Turing Way, Forrt, Servo, RSEAA24, et bien d'autres encore.

#### Donnez quelques mots-clés qui résument les thèmes de vos projets.

Infrastructure, GitOps, DevOps, Code, Documentation.

#### À quelle fréquence travaillez-vous avec des chercheurs non-codeurs ?

Pas souvent

{{< /spoiler >}}

{{< spoiler text="__:school: Parlez-nous davantage de l’organisation dans laquelle vous travaillez.__" >}}

#### Quel est leur objectif premier ?

L'objectif principal est de maintenir une communauté RSE active, diversifiée et durable tout en définissant le rôle des RSE dans le contexte de la recherche électronique, académique et professionnel, et en établissant des liens entre les individus et les organisations.

#### Combien de personnes dans votre organization sont impliquées dans le développement de logiciels de recherche (un coup de pouce est acceptable) ?

L'Association RSE d'Australie et  La Nouvelle-Zélande (RSE-AUNZ) compte actuellement 462 membres.
La RSE Asia Association a récemment été créé et compte 85 membres.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Où obtenez-vous une formation et un soutien ?__" >}}

#### De quelles communautés de pratique faites-vous partie ?

La plupart de ma formation et de mon soutien proviennent de mon implication active dans différentes communautés open source et dans diverses communautés et clubs étudiants des universités.

#### Quelle formation a eu un impact sur votre carrière actuelle ?

La participation au programme de mentorat OLS (Open Life Science) a contribué de manière significative à mon évolution de carrière.
Participer également à une bourse pratique sur la diversité et l'inclusion au PDI

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Vous considérez-vous comme un universitaire, un chercheur, un ingénieur logiciel, un technicien… ? Tout? Autre chose? Un mélange d'un ou deux termes ?__ " >}}

Alors que je me concentre actuellement sur le développement de logiciels via des contributions open source, je me vois évoluer vers le rôle d'ingénieur logiciel de recherche (RSE) à l'avenir. Cela me permet de tirer parti du codage tout en soutenant la communauté des chercheurs.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: À quels types d’obstacles êtes-vous confrontés dans votre travail ?__ " >}}


Trouver des opportunités rémunérées pour améliorer mes compétences ou mon expérience RSE

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: Quelle partie de votre travail appréciez-vous le plus ?__" >}}

Je trouve une grande satisfaction à aider les autres lorsqu'ils rencontrent des défis et à constater la croissance et les progrès au sein des communautés open source.

{{< /spoiler >}}

{{< spoiler text="__:sos: Qu’est-ce qui vous faciliterait la tâche et vous soutiendrait dans votre travail ?__" >}}

Avoir des plans et des lignes directrices plus clairs pour les tâches m'aiderait.

{{< /spoiler >}}

{{< spoiler text="__:telescope: Qu’attendez-vous avec impatience cette année ?__" >}}

J'attends avec impatience des opportunités de poursuivre un master en science des données ou dans un domaine connexe, d'organiser l'événement RSEAA24 en septembre, et de collaborer et participer à divers ateliers et événements.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Veuillez partager les conseils de carrière les plus utiles que vous avez reçus et que vous souhaitez partager avec d'autres Africains occupant des postes similaires.__" >}}

L’apprentissage ne s’arrête jamais et accepter le changement est essentiel à la croissance personnelle et professionnelle. De plus, aider les autres améliore vos connaissances et contribue à l’avancement collectif. 

{{< /spoiler >}}





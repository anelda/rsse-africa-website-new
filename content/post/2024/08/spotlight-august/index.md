---
title: "🇨🇲 August 2024: Meet Jafsia Elisee"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our August 2024 community member spotlight! In this month's edition we're introducing Jafsia Elisee from Cameroon.

# Link this post with a project
projects: []

# Date published
date: '2024-08-12T00:00:00Z'


# Date updated
lastmod: '2024-08-12T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Jafsia Elisee'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Jafsia Elisee

tags:
  - Community members
  - Electromechanics
  - Artificial Intelligence
  - Cameroon

categories:
  - Spotlights
  - Cameroon
---

Our August spotlight shines on Jafsia Elisee from Cameroon. Jafsia leads the Electromechanics and Artificial Intelligence department of MboaLab

📣 **Exciting News** 

This community spotlight is also available in [French](../../../2024/08/spotlight-august-french/).  

## In short
---

- __Preferred name and surname__: Jafsia Elisee
- __Affiliation__ (where do you work or study): Mboalab
- __Role__:   Head of Department Electromechanics/AI 
- __Email__: jafsiaelisee@gmail.com
- __LinkedIn__: https://www.linkedin.com/jafsiaelisee 
- __Twitter(X)__: https://twitter.com/euclude 
- __ORCID ID__: [0000-0002-0498-0179](https://orcid.org/0000-0001-9262-4053)
- __GitHub__: https://www.github.com/jafsia

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [GOD DID](https://www.youtube.com/watch?v=QugLZCp74GE) – _DJ Khaled ft. ft. Rick Ross, Lil Wayne, Jay-Z, John Legend, Fridayy_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?
#### Do you have postgrad qualifications? In what area?
#### What is the title of your current role?
#### Give a one-sentence summary of what you do in this role

Passionate about the intersection of technology and health, I have traveled a unique path that has led me to the forefront of innovation in Africa. After obtaining my degrees in Physics and Education at the University of Yaoundé I and the Yaoundé Higher Teacher’s Training College,  I took my first steps in the professional world as a risk analyst at BigMola. There, my creative spirit was revealed when I designed a credit scoring system based on machine learning algorithms. This experience awakened in me an insatiable curiosity for AI and its potential applications in the medical field.

Today, I enthusiastically lead the Electromechanics and Artificial Intelligence department of MboaLab in Cameroon. My work is focused on developing reliable, interpretable and responsible AI for the diagnosis of tropical diseases to contribute to improving healthcare in Africa. Apart from my main responsibilities, I am actively involved in the DIDA network and lead the OpenFlexure Microscope project in Cameroon. Despite a busy schedule, I always find time for my personal projects in physics and computer science, demonstrating my unwavering dedication to science and innovation.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?
#### How many projects do you work on?
#### Give some keywords that summarise the topics of your projects.
#### How often do you work with non-coding researchers?

My workday usually starts early, with a coffee and a quick glance at my emails. From 8 a.m., I'm in front of my screen, immersed in the code. I spend a good part of my morning working on our Mboacare project, refining the machine learning algorithms that will efficiently direct patients to the appropriate hospitals. I mainly use Python with libraries like TensorFlow and scikit-learn to develop our predictive models.

After lunch, I often focus on the Mboathoscope. It is an exciting challenge to create a wireless digital stethoscope integrating AI. I work on optimizing convolutional neural networks (CNN) for the analysis of heart and lung sounds. Between two coding sessions, I collaborate closely with our team of biologists. Their insights are valuable for refining our models and ensuring that our solutions truly meet medical needs.

At the end of the day, I usually spend time managing our department and planning our future projects. I spend an average of 8 hours a day on my machine, juggling development, data analysis and research. Although our current projects, Mboacare and Mboathoscope, occupy most of my time, I still keep an eye on our old project, the Typhoid Screener, looking for ways to improve its performance.

What I like most about my job is the diversity of technical challenges. One day I might find myself optimizing SQL queries for our patient database, the next day debugging JavaScript code for our user interface, and the next week experimenting with deep learning architectures to improve our diagnostics. It’s this combination of programming, AI and healthcare that makes each day unique and challenging.


{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?
#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

MboaLab is an organization based in Cameroon, where I have the privilege of working as head of the Electromechanics and Artificial Intelligence department. Our name, “Mboa”, means “village” or “community” in the Douala language, which perfectly reflects our philosophy of collaborative and inclusive innovation.

The primary objective of MboaLab is to develop innovative technological solutions to respond to health challenges in Africa, particularly in the field of medical diagnosis and access to care. We firmly believe that technology, and more specifically artificial intelligence, can play a crucial role in improving health systems in Africa.

When it comes to research software development, our team is relatively small but dynamic. If I had to give a rough estimate, I would say we have around 08 people directly involved in research software development. This team includes developers specialized in AI, electronic engineers for our medical devices, and data scientists.

However, what makes MboaLab unique is our collaborative approach. Although our development team is relatively small, we work closely with biological researchers, doctors, and other health experts. This interdisciplinary approach allows us to develop solutions that are not only technically advanced, but also perfectly adapted to the real needs in the field.

We strongly encourage participation from the local community and students. We regularly organize hackathons and training workshops, which allows us to involve a larger number of people in our projects on an ad hoc basis. This approach helps us cultivate local talent and raise community awareness of the importance of technological innovation in healthcare.

What excites me about MboaLab is our commitment to using technology to have a concrete positive impact on the health of African populations. Every line of code we write, every algorithm we develop, has the potential to save lives and improve access to healthcare in our region.




{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?
#### What training has had an impact on your current career?

My training and professional development have been shaped by a series of international opportunities and enriching collaborations. I am proud to be a member of several communities of practice that play a crucial role in my journey. The DIDA (DIgital Diagnostics for smarter healthcare in Africa) network has been particularly important, offering me not only valuable support but also specific training, particularly in the field of quality management of medical equipment according to the ISO 13485 standard. I am also an active member of OLS (Open Life Science), ABIC (African Bioimaging consortium) and GBI (Global Bio-Imaging), the latter having offered me the invaluable opportunity to participate in training sessions in Japan .

My career took a significant turn thanks to several significant training experiences. I received training in data science from the African Institute of Mathematical Sciences (AIMS Rwanda): I also had the honor of receiving a Fulbright scholarship from the US government, which allowed me to study at the University of Nevada, Reno. This experience significantly broadened my perspectives and strengthened my skills. Another crucial step in my journey was the six-month training I completed through the Africa Oxford Initiative. This training was decisive for the development of our Typhoid Screener solution, a project that is particularly close to my heart.

These various training experiences and affiliations with communities of practice have played a fundamental role in my professional development. They not only allowed me to acquire valuable technical skills, but also to develop an international network of collaborators and mentors. Through these opportunities, I was able to strengthen my capacity to innovate in digital health in Africa and refine my vision for using technology to improve healthcare in my region. Each experience has helped shape my understanding of the challenges and opportunities in this field, allowing me to make meaningful contributions through my work at MboaLab and my various projects.


{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I consider myself a mix of researcher and software engineer. This dual identity reflects the nature of my work and my passions. As a researcher, I am constantly looking for new knowledge and innovative solutions in the field of digital health and artificial intelligence applied to medicine. I strive to understand the complex issues facing health systems in Africa and find innovative ways to solve them.

At the same time, my software engineer side pushes me to transform these ideas into concrete and functional solutions. I enjoy designing, developing and implementing software systems that can have a real impact on population health. This combination of theoretical reflection and practical application allows me to navigate effectively between the world of academic research and that of technological development.

This duality allows me to have a holistic approach in my work at MboaLab. I can therefore not only identify problems and design theoretical solutions, but also implement them concretely through projects like Mboacare and Mboathoscope. This synergy between research and engineering is what excites me most about my work and allows me to contribute significantly to improving healthcare in Africa.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

In my work, I face several significant obstacles that sometimes make it difficult to achieve my goals. These challenges are mainly related to the infrastructure and resources available in our local context.

One of the main problems I face is the lack of reliable and complete data. As a researcher working on AI-based healthcare solutions, access to quality datasets is crucial. Unfortunately, in many areas of health in Africa, this data is either non-existent or insufficient, making it difficult to develop and validate our models.

Another major obstacle is the lack of adequate infrastructure. This includes not only the state-of-the-art equipment needed for some of our research, but also the basic facilities for developing and testing our solutions. This lack of infrastructure sometimes limits our ability to conduct certain experiments or deploy our solutions on a large scale.

Frequent power outages also pose a significant challenge. These interruptions can disrupt our work processes, especially when we are working on intensive calculations or simulations that require significant processing time. They can also damage our equipment, causing delays and additional costs.

Finally, internet quality is a recurring problem. In a field that increasingly relies on cloud computing and online collaboration, a stable and fast internet connection is essential. Unfortunately, the quality of the internet in our region is often insufficient, which can slow down our work, complicate our international collaborations and limit our access to certain online resources.

Despite these challenges, we are working on approaches that can work with limited data, we are investing in energy-saving equipment, and we are exploring ways to work efficiently even with limited internet connectivity. These challenges, although difficult, push us to innovate and develop solutions that are truly adapted to our local context.


{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

The part of my work that I enjoy the most is undoubtedly the innovative aspect and the concrete impact that I can have on health in Africa. I particularly enjoy the process of developing new technological solutions that can truly improve people's lives.

What excites me most is seeing how the abstract ideas and theoretical concepts we develop in the laboratory are transformed into tangible tools that can make a difference in the diagnosis and treatment of diseases. For example, working on projects like Mboacare and Mboathoscope gives me a sense of accomplishment. Knowing that these innovations have the potential to save lives or significantly improve the quality of healthcare in areas with limited resources is extremely rewarding. 

I also greatly enjoy the interdisciplinary collaboration that my work entails. Interacting with healthcare professionals, engineers, data scientists and AI experts allows me to have a holistic view of the problems we are trying to solve. This multidisciplinary approach stimulates my creativity and helps me see challenges from different perspectives.

I really like the research and continuous learning aspect of my job. The field of AI applied to health is evolving rapidly, and there are always new technologies to explore, new techniques to master. This constant evolution pushes me to surpass myself and to continue learning, which makes my work always stimulating and never monotonous. 

Finally, I am deeply satisfied to contribute to the technological and scientific development of Africa. Knowing that my work can inspire other young Africans to pursue careers in STEM and contribute to innovation on the continent is a source of great pride for me.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

First, increased support from local and national authorities, particularly in terms of innovation-friendly policies and facilitation of administrative processes, would help us accelerate the development and deployment of our solutions.

Second, larger and more stable funding would allow us to invest in cutting-edge equipment and more powerful computing resources. This would significantly improve our research and development work.

Third, increased opportunities for continuing education and participation in international conferences would help us stay at the forefront of advances in our field. This could include exchange programs with other research laboratories or internships at cutting-edge technology companies.

Fourth, access to larger and more diverse data sets would be crucial to improving our AI models. Strengthened partnerships with hospitals, clinics and other healthcare institutions for data collection and sharing would help us immensely in developing more accurate and reliable solutions.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

First of all, I am particularly excited about finalizing and launching our Mboacare project. We've worked hard on this telemedicine platform, and I can't wait to see it used by healthcare professionals and patients. I hope its deployment will have a significant impact on access to healthcare in our region.

I'm also very excited about the progress we're making on the Mboathoscope project. We are close to reaching an important milestone in the development of this AI-assisted diagnostic tool, and I look forward to seeing the results of our upcoming clinical tests.

This year, I am also looking forward to the opportunity to present our work at international conferences. This is always a great opportunity to share our research, get valuable feedback from the scientific community, and potentially establish new collaborations.



{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

The most useful advice I have received comes from the Africa Oxford Initiative program and can be summed up in three words: LEARN-UNLEARN-RELEARN.

**LEARN:** Adopts an agile and flexible approach. Focus on practical solutions and learn to work effectively with available resources.

**UNLEARN:** Be prepared to abandon obsolete methods and knowledge. Challenge old habits and be open to new ideas and technologies.

**RELEARN:** Commit to lifelong learning. Adapt to new discoveries and innovations to stay at the forefront in the field of AI applied to medical diagnosis.

These three principles are essential to our evolution as innovators and individuals, allowing us to overcome challenges and excel in our careers.


{{< /spoiler >}}





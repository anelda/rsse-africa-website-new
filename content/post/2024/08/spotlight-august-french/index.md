---
title: "🇨🇲 Août 2024 : Rencontrez Jafsia Elisee "

subtitle: 

# Summary for listings and search engines
summary:  Bienvenue 👋 sur notre coup de projecteur sur les membres de la communauté d'août 2024 ! Dans l'édition de ce mois-ci, nous vous présentons Jafsia Elisee du Cameroun.

# Link this post with a project
projects: []

# Date published
date: '2024-08-12T00:00:00Z'


# Date updated
lastmod: '2024-08-12T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: "Crédit d'image: Jafsia Elisee"
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Jafsia Elisee

tags:
  - Membres de la communauté
  - Électromécanique
  - Intelligence artificielle
  - Cameroon

categories:
  - Spotlights
  - Cameroon
---

Notre projecteur du mois d'août est braqué sur Jafsia Elisee du Cameroun. Jafsia dirige le département Électromécanique et Intelligence Artificielle du MboaLab

📣 **Des nouvelles passionnantes** 

Ce coup de projecteur sur la communauté est également disponible en [anglais](../../../2024/08/spotlight-august/).

## En bref
---

- __Nom et prénom souhaités__: Jafsia Elisee
- __Affiliation__ (où travaillez-vous ou étudiez-vous): Mboalab
- __Role__:   Chef du Département Électromécanique/IA 
- __Email__: jafsiaelisee@gmail.com
- __LinkedIn__: https://www.linkedin.com/jafsiaelisee 
- __Twitter(X)__: https://twitter.com/euclude 
- __ORCID ID__: [0000-0002-0498-0179](https://orcid.org/0000-0001-9262-4053)
- __GitHub__: https://www.github.com/jafsia

---


{{< spoiler text="__:musical_score: Quelle est votre chanson préférée ?__" >}}

:sound: [GOD DID](https://www.youtube.com/watch?v=QugLZCp74GE) – _DJ Khaled ft. ft. Rick Ross, Lil Wayne, Jay-Z, John Legend, Fridayy_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Pouvez-vous décrire votre parcours et votre rôle actuel ?__" >}}

#### Qu’as-tu étudié et où ?
#### Avez-vous des diplômes de troisième cycle ? Dans quel domaine ?
#### Quel est le titre de votre rôle actuel ?
#### Donnez un résumé en une phrase de ce que vous faites dans ce rôle

Passionné par l'intersection de la technologie et de la santé, J’ai parcouru un chemin unique qui m'a mené à la pointe de l'innovation en Afrique. Après avoir obtenu mes diplômes en Physique et en éducation à l'Université de Yaoundé I et à l'École Normale Supérieure de Yaoundé, J’ ai fait ses premiers pas dans le monde professionnel en tant qu'analyste de risques chez BigMola. Là-bas, mon esprit créatif s'est révélé lorsque j’ ai conçu un système de notation de crédit basé sur des algorithmes d'apprentissage automatique. Cette expérience a éveillé en moii une curiosité insatiable pour l'IA et ses applications potentielles dans le domaine médical.

Aujourd'hui, je dirige avec enthousiasme le département d'Électromécanique et d'Intelligence Artificielle du MboaLab au Cameroun. mon travail est axé sur le développement d'une IA fiable, interprétable et responsable pour le diagnostic des maladies tropicales afin de contribuer à l’amélioration des soins de santé en Afrique. En dehors de mes responsabilités principales, je m'implique activement dans le réseau DIDA et dirige  le projet OpenFlexure Microscope au Cameroun. Malgré un emploi du temps chargé, je trouve toujours du temps pour mes projets personnels en physique et en informatique, démontrant mon dévouement inébranlable à la science et à l'innovation.


{{< /spoiler >}}

{{< spoiler text="__:briefcase: Décrire une journée de travail type... Développez spécifiquement le temps que vous passez à coder__" >}}

#### Combien de temps passez-vous à coder ?
#### Sur combien de projets travaillez-vous ?
#### Donnez quelques mots-clés qui résument les thèmes de vos projets.
#### À quelle fréquence travaillez-vous avec des chercheurs non-codeurs ?

Ma journée de travail commence généralement tôt, avec un café et un rapide coup d'œil à mes e-mails. Dès 8h, je suis devant mon écran, plongé dans le code. Je passe une bonne partie de ma matinée à travailler sur notre projet Mboacare, peaufinant les algorithmes de machine learning qui permettront d'orienter efficacement les patients vers les hôpitaux appropriés. J'utilise principalement Python avec des bibliothèques comme TensorFlow et scikit-learn pour développer nos modèles prédictifs.

Après le déjeuner, je me concentre souvent sur le Mboathoscope. C'est un défi passionnant de créer un stéthoscope digital sans fil intégrant l'IA. Je travaille sur l'optimisation des réseaux de neurones convolutifs (CNN) pour l'analyse des sons cardiaques et pulmonaires. Entre deux sessions de codage, je collabore étroitement avec notre équipe de biologistes. Leurs insights sont précieux pour affiner nos modèles et assurer que nos solutions répondent réellement aux besoins médicaux.

En fin de journée, je consacre généralement du temps à la gestion de notre département et à la planification de nos projets futurs. Je passe en moyenne 8 heures par jour sur ma machine, jonglant entre le développement, l'analyse de données et la recherche. Bien que nos projets actuels, Mboacare et Mboathoscope, occupent la majeure partie de mon temps, je garde toujours un œil sur notre ancien projet, le Typhoid Screener, cherchant des moyens d'améliorer ses performances.

Ce que j'aime le plus dans mon travail, c'est la diversité des défis techniques. Un jour, je peux me retrouver à optimiser des requêtes SQL pour notre base de données de patients, le lendemain à déboguer du code JavaScript pour notre interface utilisateur, et la semaine suivante à expérimenter avec des architectures de deep learning pour améliorer nos diagnostics. C'est cette combinaison de programmation, d'IA et de santé qui rend chaque journée unique et stimulante.

{{< /spoiler >}}

{{< spoiler text="__:school: Parlez-nous davantage de l’organisation dans laquelle vous travaillez.__" >}}

#### Quel est leur objectif premier ?
#### Combien de personnes dans votre organization sont impliquées dans le développement de logiciels de recherche (un coup de pouce est acceptable) ?

MboaLab est une organisation  basée au Cameroun, où j'ai le privilège de travailler en tant que chef du département d'Électromécanique et d'Intelligence Artificielle. Notre nom, "Mboa", signifie "village" ou "communauté" en langue douala, ce qui reflète parfaitement notre philosophie d'innovation collaborative et inclusive.

L'objectif premier de MboaLab est de développer des solutions technologiques innovantes pour répondre aux défis de santé en Afrique, en particulier dans le domaine du diagnostic médical et de l'accès aux soins. Nous croyons fermement que la technologie, et plus particulièrement l'intelligence artificielle, peut jouer un rôle crucial dans l'amélioration des systèmes de santé en Afrique.

En ce qui concerne le développement de logiciels de recherche, notre équipe est relativement petite mais dynamique. Si je devais donner une estimation approximative, je dirais que nous avons environ 08 personnes directement impliquées dans le développement de logiciels de recherche. Cette équipe comprend des développeurs spécialisés en IA, des ingénieurs en électronique pour nos dispositifs médicaux, et des data scientists.

Cependant, ce qui rend MboaLab unique, c'est notre approche collaborative. Bien que notre équipe de développement soit relativement petite, nous travaillons en étroite collaboration avec des chercheurs en biologie, des médecins, et d'autres experts en santé. Cette approche interdisciplinaire nous permet de développer des solutions qui sont non seulement techniquement avancées, mais aussi parfaitement adaptées aux besoins réels du terrain.

De plus, nous encourageons vivement la participation de la communauté locale et des étudiants. Nous organisons régulièrement des hackathons et des ateliers de formation, ce qui nous permet d'impliquer un nombre plus important de personnes dans nos projets de manière ponctuelle. Cette approche nous aide à cultiver les talents locaux et à sensibiliser la communauté à l'importance de l'innovation technologique dans le domaine de la santé.

En fin de compte, ce qui me passionne chez MboaLab, c'est notre engagement à utiliser la technologie pour avoir un impact positif concret sur la santé des populations africaines. Chaque ligne de code que nous écrivons, chaque algorithme que nous développons, a le potentiel de sauver des vies et d'améliorer l'accès aux soins de santé dans notre région.



{{< /spoiler >}}

{{< spoiler text="__:bulb: Où obtenez-vous une formation et un soutien ?__" >}}

#### De quelles communautés de pratique faites-vous partie ?
#### Quelle formation a eu un impact sur votre carrière actuelle ?

Ma formation et mon développement professionnel ont été façonnés par une série d'opportunités internationales et de collaborations enrichissantes. Je suis fier d'être membre de plusieurs communautés de pratique qui jouent un rôle crucial dans mon parcours. Le réseau DIDA (DIgital Diagnostics for smarter healthcare in Africa) a été particulièrement important, m'offrant non seulement un soutien précieux mais aussi des formations spécifiques, notamment dans le domaine de la gestion de la qualité des équipements médicaux selon la norme ISO 13485. Je suis également membre actif de OLS (Open Life Science),  l'ABIC (African Bioimaging consortium) et de GBI (Global Bio-Imaging), cette dernière m'ayant offert l'opportunité inestimable de participer à des sessions de formation au Japon.

Ma carrière a pris un tournant significatif grâce à plusieurs expériences de formation marquantes. J’ai subi une formation en data science de la part de l’African Institute of Mathematical Sciences (AIMS Rwanda): J'ai aussi  eu l'honneur de recevoir une bourse Fulbright du gouvernement américain, qui m'a permis d'étudier à l'Université du Nevada, Reno. Cette expérience a considérablement élargi mes perspectives et renforcé mes compétences. Une autre étape cruciale de mon parcours a été la formation de six mois que j'ai suivie dans le cadre de l'Oxford African Initiative. Cette formation a été déterminante pour le développement de notre solution Typhoid Screener, un projet qui me tient particulièrement à cœur.

Ces diverses expériences de formation et ces affiliations à des communautés de pratique ont joué un rôle fondamental dans mon évolution professionnelle. Elles m'ont non seulement permis d'acquérir des compétences techniques précieuses, mais aussi de développer un réseau international de collaborateurs et de mentors. Grâce à ces opportunités, j'ai pu renforcer ma capacité à innover dans le domaine de la santé numérique en Afrique et affiner ma vision de l'utilisation de la technologie pour améliorer les soins de santé dans ma région. Chaque expérience a contribué à façonner ma compréhension des défis et des opportunités dans ce domaine, me permettant d'apporter une contribution significative à travers mon travail au MboaLab et mes divers projets.



{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Vous considérez-vous comme un universitaire, un chercheur, un ingénieur logiciel, un technicien… ? Tout? Autre chose? Un mélange d'un ou deux termes ?__ " >}}

Je me considère comme un mélange de chercheur et d'ingénieur logiciel. Cette double identité reflète bien la nature de mon travail et de mes passions. En tant que chercheur, je suis constamment à la recherche de nouvelles connaissances et de solutions innovantes dans le domaine de la santé numérique et de l'intelligence artificielle appliquée à la médecine. Je m'efforce de comprendre les problèmes complexes auxquels sont confrontés les systèmes de santé en Afrique et de trouver des moyens novateurs de les résoudre.

En même temps, mon côté ingénieur logiciel me pousse à transformer ces idées en solutions concrètes et fonctionnelles. Je prends plaisir à concevoir, développer et implémenter des systèmes logiciels qui peuvent avoir un impact réel sur la santé des populations. Cette combinaison de réflexion théorique et d'application pratique me permet de naviguer efficacement entre le monde de la recherche académique et celui du développement technologique.

Cette dualité me permet d'avoir une approche holistique dans mon travail au MboaLab. Je peux ainsi non seulement identifier les problèmes et concevoir des solutions théoriques, mais aussi les mettre en œuvre concrètement à travers des projets comme Mboacare et Mboathoscope. Cette synergie entre recherche et ingénierie est ce qui me passionne le plus dans mon travail et me permet de contribuer de manière significative à l'amélioration des soins de santé en Afrique.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: À quels types d’obstacles êtes-vous confrontés dans votre travail ?__ " >}}

Dans mon travail, je suis confronté à plusieurs obstacles significatifs qui rendent parfois difficile la réalisation de mes objectifs. Ces défis sont principalement liés à l'infrastructure et aux ressources disponibles dans notre contexte local.

L'un des principaux problèmes auxquels je fais face est l'absence de données fiables et complètes. En tant que chercheur travaillant sur des solutions de santé basées sur l'IA, l'accès à des ensembles de données de qualité est crucial. Malheureusement, dans de nombreux domaines de la santé en Afrique, ces données sont soit inexistantes, soit insuffisantes, ce qui complique le développement et la validation de nos modèles.

Un autre obstacle majeur est le manque d'infrastructure adéquate. Cela inclut non seulement les équipements de pointe nécessaires pour certaines de nos recherches, mais aussi les installations de base pour le développement et le test de nos solutions. Ce manque d'infrastructure limite parfois notre capacité à mener certaines expériences ou à déployer nos solutions à grande échelle.

Les coupures de courant fréquentes représentent également un défi de taille. Ces interruptions peuvent perturber nos processus de travail, en particulier lorsque nous travaillons sur des calculs intensifs ou des simulations qui nécessitent un temps de traitement important. Elles peuvent aussi endommager nos équipements, ce qui entraîne des retards et des coûts supplémentaires.

Enfin, la qualité de l'internet est un problème récurrent. Dans un domaine qui repose de plus en plus sur le cloud computing et la collaboration en ligne, une connexion internet stable et rapide est essentielle. Malheureusement, la qualité de l'internet dans notre région est souvent insuffisante, ce qui peut ralentir notre travail, compliquer nos collaborations internationales et limiter notre accès à certaines ressources en ligne.

Malgré ces défis, nous travaillons sur des approches qui peuvent fonctionner avec des données limitées, nous investissons dans des équipements de sauvegarde d'énergie, et nous explorons des moyens de travailler efficacement même avec une connectivité internet limitée. Ces défis, bien que difficiles, nous poussent à innover et à développer des solutions qui sont véritablement adaptées à notre contexte local.


{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: Quelle partie de votre travail appréciez-vous le plus ?__" >}}

La partie de mon travail que j'apprécie le plus est sans doute l'aspect innovant et l'impact concret que je peux avoir sur la santé en Afrique. J'aime particulièrement le processus de développement de nouvelles solutions technologiques qui peuvent réellement améliorer la vie des gens.

Ce qui me passionne le plus, c'est de voir comment les idées abstraites et les concepts théoriques que nous développons en laboratoire se transforment en outils tangibles qui peuvent faire une différence dans le diagnostic et le traitement des maladies. Par exemple, travailler sur des projets comme Mboacare et Mboathoscope me donne un sentiment d'accomplissement. Savoir que ces innovations peuvent potentiellement sauver des vies ou améliorer significativement la qualité des soins de santé dans des régions où les ressources sont limitées est extrêmement gratifiant.

J'apprécie également énormément la collaboration interdisciplinaire que mon travail implique. Interagir avec des professionnels de la santé, des ingénieurs, des data scientists et des experts en IA me permet d'avoir une vision holistique des problèmes que nous essayons de résoudre. Cette approche multidisciplinaire stimule ma créativité et m'aide à voir les défis sous différents angles.

 J'aime beaucoup l'aspect de recherche et d'apprentissage continu dans mon travail. Le domaine de l'IA appliquée à la santé évolue rapidement, et il y a toujours de nouvelles technologies à explorer, de nouvelles techniques à maîtriser. Cette constante évolution me pousse à me surpasser et à continuer d'apprendre, ce qui rend mon travail toujours stimulant et jamais monotone.

Enfin, je suis profondément satisfait de contribuer au développement technologique et scientifique de l'Afrique. Savoir que mon travail peut inspirer d'autres jeunes africains à poursuivre des carrières dans les STEM et à contribuer à l'innovation sur le continent est une source de grande fierté pour moi.


{{< /spoiler >}}

{{< spoiler text="__:sos: Qu’est-ce qui vous faciliterait la tâche et vous soutiendrait dans votre travail ?__" >}}

Premièrement, un soutien accru de la part des autorités locales et nationales, notamment en termes de politiques favorables à l'innovation et de facilitation des processus administratifs, nous aiderait à accélérer le développement et le déploiement de nos solutions.

Deuxièmement,  un financement plus important et plus stable nous permettrait d'investir dans des équipements de pointe et des ressources de calcul plus puissantes. Cela améliorerait considérablement notre travail de recherche et de développement.

Troisièmement, des opportunités accrues de formation continue et de participation à des conférences internationales nous aideraient à rester à la pointe des avancées dans notre domaine. Cela pourrait inclure des programmes d'échange avec d'autres laboratoires de recherche ou des stages dans des entreprises technologiques de pointe.
Quatrièmement, l'accès à des ensembles de données plus larges et plus diversifiés serait crucial pour améliorer nos modèles d'IA. Des partenariats renforcés avec des hôpitaux, des cliniques et d'autres institutions de santé pour la collecte et le partage de données nous aideraient énormément dans le développement de solutions plus précises et plus fiables.

{{< /spoiler >}}

{{< spoiler text="__:telescope: Qu’attendez-vous avec impatience cette année ?__" >}}

Tout d'abord, je suis particulièrement enthousiaste à l'idée de finaliser et de lancer notre projet Mboacare. Nous avons travaillé dur sur cette plateforme de télémédecine, et je suis impatient de la voir utilisée par les professionnels de santé et les patients. J'espère que son déploiement aura un impact significatif sur l'accès aux soins de santé dans notre région.

Je suis également très excité par les avancées que nous faisons sur le projet Mboathoscope. Nous sommes sur le point de franchir une étape importante dans le développement de cet outil de diagnostic assisté par IA, et j'ai hâte de voir les résultats de nos prochains tests cliniques.

Cette année, j'attends aussi avec impatience la possibilité de présenter nos travaux lors de conférences internationales. C'est toujours une excellente opportunité pour partager nos recherches, obtenir des retours précieux de la communauté scientifique, et potentiellement établir de nouvelles collaborations.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Veuillez partager les conseils de carrière les plus utiles que vous avez reçus et que vous souhaitez partager avec d'autres Africains occupant des postes similaires.__" >}}

Les conseils les plus utiles que j'ai reçus viennent du programme African Oxford Initiative et se résument en trois mots : LEARN-UNLEARN-RELEARN (APPRENDRE-DÉSAPPRENDRE-RÉAPPRENDRE).

**APPRENDRE (LEARN):** Adopte une approche agile et flexible. Concentre-toi sur les solutions pratiques et apprends à travailler efficacement avec les ressources disponibles.

**DÉSAPPRENDRE (UNLEARN):** Sois prêt à abandonner les méthodes et connaissances obsolètes. Remets en question les anciennes habitudes et sois ouvert aux nouvelles idées et technologies.

**RÉAPPRENDRE (RELEARN):** Engage-toi dans un apprentissage continu. Adapte-toi aux nouvelles découvertes et innovations pour rester à la pointe dans le domaine de l'IA appliquée au diagnostic médical.

Ces trois principes sont essentiels pour notre évolution en tant qu'innovateurs et individus, nous permettant de surmonter les défis et d'exceller dans nos carrières.


{{< /spoiler >}}





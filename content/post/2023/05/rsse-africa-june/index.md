---
title: "ChatGPT for teaching and/or learning to program"
subtitle: 

# Summary for listings and search engines
summary: This event is a collaboration between RSSE Africa and RSE Australia/New Zealand. It is an excellent opportunity to learn about LLMs in teaching and learning, and we will also meet colleagues from the other side of the world!


# Link this post with a project
projects: []

# Date published
date: '2023-05-25T00:00:00Z'

# Date updated
lastmod: '2023-05-25T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: ''
  focal_point: ''
  placement: 
  preview_only: false

authors:
  - Anelda van der Walt

tags:
  - ChatGPT
  - LLM
  - teaching
  - learning

categories:
  - Community meetup
---

{{< cta cta_text="Click here to register!" cta_link="https://docs.google.com/forms/d/e/1FAIpQLSciuba_o9bkVT1PAfsoG-i1E9_rs8kkMP5KFr6DNYNMB8_o3Q/viewform?usp=sf_link" cta_new_tab="true" >}}


[Large language models](https://en.wikipedia.org/wiki/Large_language_model) (LLMs) like [ChatGPT](https://openai.com/blog/chatgpt) are disrupting the world as we know it, even the world of learning or teaching to program.

In our RSSE Africa Community Meetup held in April 2023, one of the community members brought up the question of ChatGPT in the discussion. As many of our community members are involved in training at various levels, we were wondering what the benefits and pitfalls of using this new resource in the capacity-building programmes we develop will be.

By chance, we learned about the work of [Prof Paul Denny](https://profiles.auckland.ac.nz/p-denny) on the topic. Paul is an Associate Professor in the School of Computer Science at the University of Auckland. His research interests include developing and evaluating tools for supporting collaborative learning, particularly involving student-generated resources, and exploring how students engage with digital learning environments. Together with colleagues from across the globe, Paul has been looking at using ChatGPT in various settings, including [Bioinformatics task automation](https://arxiv.org/pdf/2303.13528.pdf), [teaching Computer Science](https://dl.acm.org/doi/10.1145/3545947.3573358), and more.

During the same time, we met Precious Bob-Manuel, a recent graduate in political science from Nigeria. Precious is self-learning JavaScript with the help of ChatGPT.

We invited Paul and Precious to join us at the RSSE Africa Community Meetup in June, to share their experiences and start a conversation about how we can benefit from LLMs in our work and training.

This event is a collaboration between RSSE Africa and RSE Australia/New Zealand. It is an excellent opportunity to learn about LLMs in teaching and learning, and we will also meet colleagues from the other side of the world!

Looking forward to seeing you there!


{{< cta cta_text="Register now!" cta_link="https://docs.google.com/forms/d/e/1FAIpQLSciuba_o9bkVT1PAfsoG-i1E9_rs8kkMP5KFr6DNYNMB8_o3Q/viewform?usp=sf_link" cta_new_tab="true" >}}

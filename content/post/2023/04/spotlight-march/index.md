---
title: "🇲🇼 March 2023: Meet Clinton Nkolokosa"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our third community  member spotlight! In this month's edition we're introducing Clinton Nkolokosa from Malawi.

# Link this post with a project
projects: []

# Date published
date: '2023-04-14T00:00:00Z'


# Date updated
lastmod: '2023-04-14T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Clinton Nkolokosa'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Clinton Nkolokosa

tags:
  - Community members
  - Geographical Information Science
  - GIS
  - Public health
  - Ecology
  - Malawi

categories:
  - Malawi
  - Spotlights
---

Our second spotlight shines on Clinton Nkolokosa from Malawi. Clinton is a geospatial analyst solving pressing environmental problems at the interface between public health and ecology. 

## In short
---

- __Preferred name and surname__: Clinton Nkolokosa
- __Affiliation__ (where do you work or study): Malawi-Liverpool-Wellcome Trust
- __Role__: Geospatial Analyst
- __Email__: cnkolokosa@mlw.mw

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Ballad of Lonely Ghosts](https://youtu.be/XLZ_LCadWmE) – _Color Therapy ft Hammock_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

- What did you study?
- Do you have postgrad qualifications? In what area?
- What is the title of your current role?
- Give a one-sentence summary of what you do in this role

I studied natural resources management, with a specialty in biodiversity conservation, at the Lilongwe University of Agriculture and Natural Resources (LUANAR), Malawi. This course provided a springboard for attaining a master’s in Geographical Information Science (GIS) at Sheffield Hallam University, UK. And now I work as a geospatial analyst , applying my spatial skills in solving pressing environmental problems at the interface between public health and ecology. 


{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

- How much time do you spend coding?
- How many projects do you work on?
- Give some keywords that summarise the topics of your projects.
- How often do you work with non-coding researchers?

On a typical day, I split my time between coding in R, Python and Javascript in Google Earth Engine (5 hours at the very least), going through Stack Overflow  questions and answers, and writing and reading manuscripts. I usually work on two projects in a day - one in the morning and another in the afternoon. My projects can be summarized by these three keywords: floods, snails and malaria. Every now and then I work with non-coding researchers (lab-, hospital- and community-based), given my research work is transdisciplinary.

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

- What is their primary objective?
- How many people in your organisation are involved in research software development (a thumbsuck is okay)?

I work at Malawi-Liverpool-Wellcome Trust, a non-profit research institution whose remit is to study major public health issues and train the next generation of researchers. About 50 people I think are involved in research software development- there is a whole statistics department and data department that provides geospatial statistical support and develops bespoke data collection, spatial and statistical toolkits, respectively.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

- Which communities of practice are you part of?
- What training has had an impact on your current career?

I often obtain my training through in-house short training courses, African Drone and Data Academy powered by UNICEF and DataCamp, and get support from my peers, an R community at MLW and QGIS community. Definitely my Master’s training has had a big impact on my career. The structured training has well equipped me with a suite of advanced spatial analysis and critical thinking skills.

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}


I see myself juggling a full time academic job and research work.  Good to be busy with sharing knowledge and applying it I think.

{{< /spoiler >}}


{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

Siloed data, lack of mentoring and collaboration and below standard computing equipment.


{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Being in the field and spending time interacting with the local communities. For example, hearing their lived experience, hunting vectors (mosquito and snails) together and sharing knowledge motivates me to continue using science to improve local people’s lives 

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

More open access public health and earth observation data and more advanced African centred training institutions.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

Ascertaining the vector-borne diseases in Malawi made worse by climate change.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

One thing that is difficult to explain is the importance of supportive and collaborative networks. I have always tried to play an open hand and encourage, though I realise that sometimes being secretive and destructive also has its measure. Those that follow the second path do well for 5-8 years then dry up as they are washed up on a dry shore without any option. So try and take the former path – you will have people that would welcome your presence, always glad to see you and will try to help (in some way) when asked for rather than coerced.


{{< /spoiler >}}





---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 5: Testing Research Code"
subtitle: |
    <br>

    <h3>20 February 2025 @ 8:30 - 10:00 am UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Testing+Research+Code&iso=20250220T0830&p1=1440&ah=1&am=30">(your local time)</a></h3>

    <br>

    Have you ever written tests for your research code?

    In this month’s episode we’ll explore why research code should include tests, what the purpose of tests are, different types of tests and how to design them, where to start with testing and finally how to automate testing.

design:
    columns: "1"
---



---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 4: Documentation for Research Code"
subtitle: |
    <br>

    <h3>23 January 2025 @ 8:30 - 10:00 am UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Documentation+for+Research+Code&iso=20250123T0830&p1=1440&ah=1&am=30">(your local time)</a></h3>

    <br>

    Do you dread writing documentation for the code you develop as part of your research projects?

    This month we will focus on the motivation for creating documentation for research code, different types of documentation, some small additions that can already make a world of difference to the usability of your code (for yourself and others!) for example, in-code comments and README files. We’ll also look at some more advanced documentation options.

design:
    columns: "1"
---



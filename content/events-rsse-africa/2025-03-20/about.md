---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 6: Research Software Funding"
subtitle: |
    <br>

    <h3>20 March 2025 @ 8:30 - 10:00 am UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Research+Software+Funding&iso=20250320T0830&p1=1440&ah=1&am=30">(your local time)</a></h3>

    <br>

    The previous sessions in this series provided ways in which research code can be improved to make it more fundable, sustainable, and support reproducibility.

    In this final episode, we’ll look at different funding opportunities for research software. Who funds research software, what are they interested in, and where to get more information.    

design:
    columns: "1"
---



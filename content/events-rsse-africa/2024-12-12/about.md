---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 3: Open Research Software"
subtitle: |
    <br>

    <h3>12 December 2024 @ 8:30 - 10:00 am UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?iso=20241212T0830&p1=1440&ah=1&am=30">(your local time)</a></h3>

    <br>

    Are you ready to get some credit for the coding you’ve been doing as part of your research?

    This episode will focus on ways to make your code contribute to your resumé. We will look at how you can share your code with the world (including various software licensing options), get persistent identifiers and make your code citable, and even how you can publish your code as an article!

design:
    columns: "1"
---



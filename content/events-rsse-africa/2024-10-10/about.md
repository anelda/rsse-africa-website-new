---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 1: A Conversation with Researchers Who Code"
subtitle: |
    <br>

    <h3>10 October 2024 @ 8:30 - 10:00 am UTC </h3>

    <br>

    Join us on [International Research Software Engineering Day](https://society-rse.org/international-rse-day-october-10th-2024/) to connect with the global RSE community.
    
    In this inaugeral meetup we met several researchers who spend a significant part of their day programming. We heard about their projects, preferred programming languages, the teams they work in, their career journeys, and what advice they can share to fellow research software developers.

weight: 10

design:
    columns: "1"
---


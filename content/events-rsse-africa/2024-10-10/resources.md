---
widget: blank
headless: true  # This file represents a page section.
title: "Meetup Resources"
subtitle: 

weight: 30   

design:
    columns: "1"
---

<br>

<div class="container">
    <div class="row">
        <div class="col-6">
            <p align="center">
                Recording coming soon...
            </p>
        </div>
        <div class="col-6">
            <p align="center">
                Access our <a href="https://doi.org/10.5281/zenodo.13946139" target="_blank">Resources Sheet</a>
                <br>
                <a href="https://doi.org/10.5281/zenodo.13946139" target="_blank"><img src="resource-sheet.png" width="40%"></a>
            </p>
        </div>
    </div>
</div>
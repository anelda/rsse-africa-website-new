# RSSE Africa website

This is the content for the website for https://rsse.africa. The content here is deployed on Netlify
using the [Hugo](https://gohugo.io/) static site generator. It is built off the [Wowchemy templates](https://wowchemy.com/).

This site's content is licensed under a [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) license.